# Use an official Maven image as the base image for the Maven build stage
FROM maven:latest AS maven-builder

# Set the working directory in the container
WORKDIR /app

# Copy the Maven artifact from the CI/CD pipeline into the container
COPY test.jar .

# There's no need to run Maven here since the JAR file is already built in the CI/CD pipeline

# Use a lightweight OpenJDK-based image to run the application in the final stage
FROM openjdk:17-jdk-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the Maven artifact from the maven-builder stage into this container
COPY --from=maven-builder /app/test.jar .

# Expose the port the app runs on (if necessary)
# EXPOSE 8080

# Define the command to run the application
CMD ["java", "-jar", "test.jar"]
